﻿namespace TestInterviewProject;

public class Triangle : IFigure
{
    private readonly double _maxSide;
    private readonly List<double> _sides;

    public Triangle(double side1, double side2, double side3)
    {
        var sides = new List<double>() { side1, side2, side3 };
        if (sides.Any(x => x <= 0))
        {
            throw new Exception();
        }

        _maxSide = sides.Max();
        if (_maxSide >= sides.Sum() - _maxSide)
        {
            throw new Exception();
        }

        _sides = sides;
    }

    public double GetSquare()
    {
        var perimeter = _sides.Sum() / 2;
        var aggregate = _sides.Select(x => perimeter - x)
            .Aggregate((x, next) => x * next);
        return Math.Sqrt(perimeter * aggregate);
    }

    public bool IsRightTriangle()
    {
        if (_sides.Count(x => x == _maxSide) > 1)
            return false;
        
        return _maxSide - Math.Sqrt(_sides.OrderByDescending(x => x).Skip(1)
            .Select(x => Math.Pow(x, 2))
            .Aggregate((x, next) => x + next)) == 0;
    }
}