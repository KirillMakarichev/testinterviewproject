﻿namespace TestInterviewProject;

public interface IFigure
{
    double GetSquare();
}