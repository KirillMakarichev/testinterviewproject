using NUnit.Framework;
using TestInterviewProject;

namespace FiguresTests;

public class TriangleTests
{
    //тест разностороннего
    [TestCase(3, 4, 5, ExpectedResult = 6)]

    //тест равностороннего
    [TestCase(2, 2, 2, ExpectedResult = 1.732051)]

    //тест равнобедренного
    [TestCase(15, 15, 24, ExpectedResult = 108)]
    public double SuccessfulTriangle(double side1, double side2, double side3)
    {
        var triangle = new Triangle(side1, side2, side3);
        return Math.Round(triangle.GetSquare(), 6);
    }

    //тест несуществующего треугольника
    [TestCase(2, 2, 5)]

    //тест с нулевой стороной
    [TestCase(0, 3, 2)]

    //тест с отрицательной стороной
    [TestCase(-15, 15, 24)]
    public void InvalidTriangle(double side1, double side2, double side3)
    {
        Assert.That(() => new Triangle(side1, side2, side3), Throws.TypeOf<Exception>());
    }


    //тест разностороннего
    [TestCase(3, 4, 5, ExpectedResult = true)]
    
    //тест разностороннего
    [TestCase(3, 6, 5, ExpectedResult = false)]

    //тест равностороннего
    [TestCase(2, 2, 2, ExpectedResult = false)]

    //тест равнобедренного - гипотенуза должна быть a*sqrt(2)
    [TestCase(8, 8, 11.313708498984761, ExpectedResult = true)]

    //тест равнобедренного
    [TestCase(8, 8, 11, ExpectedResult = false)]
    public bool IsRightTriangle(double side1, double side2, double side3)
    {
        var triangle = new Triangle(side1, side2, side3);
        return triangle.IsRightTriangle();
    }
}