using NUnit.Framework;
using TestInterviewProject;

namespace FiguresTests;

public class CircleTests
{
    [TestCase(5, ExpectedResult = 78.539816)]
    [TestCase(0.5, ExpectedResult = 0.785398)]
    public double SuccessfulCircle(double radius)
    {
        var circle = new Circle(radius);
        return Math.Round(circle.GetSquare(), 6);
    }

    //тест нулевого радиуса
    [TestCase(0)]
    
    //тест отрицательного радиуса
    [TestCase(-5)]
    public void InvalidCircle(double radius)
    {
        Assert.That(() => new Circle(radius), Throws.TypeOf<Exception>());
    }
}